<?php

/**
 * Provide a dashboard view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       http://huddleapp.me
 * @since      1.0.0
 *
 * @package    Ucl
 * @subpackage Ucl/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<div class="wrap">
	<h2>User Control Limit</h2>

	<h3>Registred User (from <?php echo $limit1 ?>): <?php echo $isReachedLimit["totalUsers"]; ?></h3>
	<h3>User Limit Reached? <?php echo ($isReachedLimit["isReachedLimit"]) ? "Sí" : "No"; ?></h3>

	<form method="post" action="#" novalidate="novalidate">
		<?php settings_fields('UCL'); ?>
		<table class="form-table">
		<tbody>
			<tr>
				<th scope="row"><label for="registerActive">Is user register active?:</label></th>
				<td>
					<input name="registerActive" type="checkbox" id="registerActive"<?php echo ($registerActive) ? " checked='checked'" : "" ?>>
					<!-- <p class="description">El registro y el conteo de límite de usuario están activos.</p> -->
				</td>
			<tr>
				<th scope="row"><label for="userLimitOption">User Limit</label></th>
				<td><input name="userLimitOption" type="text" id="userLimitOption" value="<?php echo $userLimitOption ?>" class="regular-text"></td>
			</tr>
			<tr>
				<th scope="row"><label for="limit1">Begin user counting from:</label></th>
				<td>
					<input name="limit1" type="date" id="limit1" value="<?php echo $limit1 ?>" class="regular-text">
					<!-- <p class="description">In a few words, explain what this site is about.</p> -->
				</td>
			</tr>
		</tbody>
	</table>

	<p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="Guardar Cambios"></p></form>

</div>