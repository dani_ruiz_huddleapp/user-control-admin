<?php

/**
 * The dashboard-specific functionality of the plugin.
 *
 * @link       http://huddleapp.me
 * @since      1.0.0
 *
 * @package    Ucl
 * @subpackage Ucl/admin
 */

/**
 * The dashboard-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the dashboard-specific stylesheet and JavaScript.
 *
 * @package    Ucl
 * @subpackage Ucl/admin
 * @author     huddleapp.me <dani.ruiz@huddleapp.me>
 */
class Ucl_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $ucl    The ID of this plugin.
	 */
	private $ucl;
	/**
	 * The name of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $name    The name of this plugin.
	 */
	private $name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @var      string    $ucl       The name of this plugin.
	 * @var      string    $name       The name of this plugin.
	 * @var      string    $version    The version of this plugin.
	 */
	public function __construct( $ucl, $name, $version ) {

		$this->ucl = $ucl;
		$this->name = $name;
		$this->version = $version;

		add_action( 'admin_menu', array( $this, 'add_plugin_admin_menu' ) );

	}

	/**
	 * Register the stylesheets for the Dashboard.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Ucl_Admin_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Ucl_Admin_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->ucl, plugin_dir_url( __FILE__ ) . 'css/ucl-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the dashboard.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Ucl_Admin_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Ucl_Admin_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->ucl, plugin_dir_url( __FILE__ ) . 'js/ucl-admin.js', array( 'jquery' ), $this->version, false );

	}

	/**
	 * Register the administration menu for this plugin into the WordPress Dashboard menu.
	 *
	 * @since    1.0.0
	 */
	public function add_plugin_admin_menu() {

		$this->plugin_screen_hook_suffix = add_menu_page(
			$this->name,
			$this->name,
			'manage_options',
			$this->ucl,
			array( $this, 'display_plugin_admin_page' )
		);

		add_menu_page( $page_title, $menu_title, $capability, $menu_slug, $function, $icon_url, $position );

	}


	/**
	 * Render the settings page for this plugin.
	 *
	 * @since    1.0.0
	 */
	public function display_plugin_admin_page() {
		if(array_key_exists("action", $_POST) && $_POST["action"] == "update"){
			//registerActive
			if(array_key_exists("registerActive", $_POST)){
				add_option( "register-active", $_POST["registerActive"] );
				update_option( "register-active", $_POST["registerActive"] );
			}else{
				delete_option( "register-active");
			}
			//userLimitOption
			if(array_key_exists("userLimitOption", $_POST) && strlen($_POST["userLimitOption"]) > 0){
				add_option( "user-limit-option", $_POST["userLimitOption"] );
				update_option( "user-limit-option", $_POST["userLimitOption"] );
			}
			//limit1
			if(array_key_exists("limit1", $_POST) && strlen($_POST["limit1"]) > 0){
				add_option( "user-limit-date", $_POST["limit1"] );
				update_option( "user-limit-date", $_POST["limit1"] );
			}
		}
		$userLimitOption = get_option( "user-limit-option" );
		$registerActive = get_option( "register-active" );
		$limit1 = get_option( "user-limit-date" );

		$isReachedLimit = Ucl_Limit_User::isReachedLimit();

		include_once( plugin_dir_path(  __FILE__  )  . 'partials/ucl-admin-display.php' );
	}

}
