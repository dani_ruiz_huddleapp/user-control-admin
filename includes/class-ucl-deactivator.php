<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://huddleapp.me
 * @since      1.0.0
 *
 * @package    Ucl
 * @subpackage Ucl/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Ucl
 * @subpackage Ucl/includes
 * @author     huddleapp.me <dani.ruiz@huddleapp.me>
 */
class Ucl_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
