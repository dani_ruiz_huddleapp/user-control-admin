<?php

/**
 * Fired during plugin activation
 *
 * @link       http://huddleapp.me
 * @since      1.0.0
 *
 * @package    Ucl
 * @subpackage Ucl/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Ucl
 * @subpackage Ucl/includes
 * @author     huddleapp.me <dani.ruiz@huddleapp.me>
 */
class Ucl_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
