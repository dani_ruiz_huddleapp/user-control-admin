<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://huddleapp.me
 * @since      1.0.0
 *
 * @package    Ucl
 * @subpackage Ucl/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the dashboard-specific stylesheet and JavaScript.
 *
 * @package    Ucl
 * @subpackage Ucl/public
 * @author     huddleapp.me <dani.ruiz@huddleapp.me>
 */
class Ucl_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $ucl    The ID of this plugin.
	 */
	private $ucl;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @var      string    $ucl       The name of the plugin.
	 * @var      string    $version    The version of this plugin.
	 */
	public function __construct( $ucl, $version ) {

		$this->ucl = $ucl;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Ucl_Public_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Ucl_Public_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->ucl, plugin_dir_url( __FILE__ ) . 'css/ucl-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Ucl_Public_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Ucl_Public_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->ucl, plugin_dir_url( __FILE__ ) . 'js/ucl-public.js', array( 'jquery' ), $this->version, false );

	}

}
