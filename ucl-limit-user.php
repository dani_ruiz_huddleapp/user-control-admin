<?php
	class Ucl_Limit_User {

		function __construct() {

		}

		public static function isReachedLimit( ){
			global $wpdb, $wp_roles;

			$limiteUsuarios = get_option( "user-limit-option" );
			$registroActivo = get_option( "register-active" );
			$fecha = get_option( "user-limit-date" );

			$resp = array(
				"totalUsers" => 0,
				"isReachedLimit" => false
			);
			if(!$registroActivo)
				$resp["isReachedLimit"] = true;

			// Initialize
			$id = get_current_blog_id();
			$blog_prefix = $wpdb->get_blog_prefix($id);

			if ( ! isset( $wp_roles ) )
				$wp_roles = new WP_Roles();

			$avail_roles = array("subscriber");

			// Build a CPU-intensive query that will return concise information.
			$select_count = array();
			foreach ( $avail_roles as $this_role ) {
				$select_count[] = "COUNT(NULLIF(`meta_value` LIKE '%\"" . like_escape( $this_role ) . "\"%', false))";
			}
			$select_count = implode(', ', $select_count);

			// Add the meta_value index to the selection list, then run the query.
			$sql = "SELECT $select_count, COUNT(*) FROM $wpdb->usermeta INNER JOIN apps_users ON apps_usermeta.`user_id` = apps_users.ID WHERE meta_key = '{$blog_prefix}capabilities' AND apps_users.`user_registered` >= '{$fecha}'";

			$row = $wpdb->get_row( $sql, ARRAY_N );

			// Run the previous loop again to associate results with role names.
			$col = 0;
			$role_counts = array();
			foreach ( $avail_roles as $this_role ) {
				$count = (int) $row[$col++];
				if ($count > 0) {
					$role_counts[$this_role] = $count;
				}
			}

			// Get the meta_value index from the end of the result set.
			$total_users = (int) $row[$col];

			$result['total_users'] = $total_users;
			$result['avail_roles'] =& $role_counts;

			$resp["totalUsers"] = $total_users;
			$resp["isReachedLimit"] = ($total_users >= $limiteUsuarios);

			return $resp;
		}
	}